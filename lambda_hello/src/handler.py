import requests
import os

adr = os.environ.get('adr')

def lambda_handler(*_):

    response = requests.get(adr)

    return {
        "statusCode": 200,
        "body": response.json()
    }
