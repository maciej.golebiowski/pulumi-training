import pulumi
import pulumi_aws as aws
from pulumi_aws.iam import role

from infrastructure import hello_role

aws.lambda_.Function(
    "hello",
    role=hello_role.arn,
    handler="handler.lambda_handler",
    runtime="python3.8",
    memory_size=256,
    code=pulumi.asset.FileArchive("lambda_hello/src"),
    environment=aws.lambda_.FunctionEnvironmentArgs(variables={
        "PYTHONPATH": "/var/task/lib",
        "adr": "https://jsonplaceholder.typicode.com/todos/1"
    })
)
