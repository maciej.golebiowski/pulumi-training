import json
import pulumi

import pulumi_aws as aws

hello_role = aws.iam.Role(
    "hello-role",
    assume_role_policy=json.dumps({
        "Version": "2012-10-17",
        "Statement": [{
            "Action": "sts:AssumeRole",
            "Effect": "Allow",
            "Sid": "",
            "Principal": {
                "Service": "lambda.amazonaws.com",
            },
        }],
    }),
    tags={
        "Name": "hello",
    })


aws.iam.RolePolicy(
    "hello-policy",
    role=hello_role.id,
    policy=json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {

                    "Effect": "Allow",
                    "Action": [
                        "logs:CreateLogStream",
                        "logs:CreateLogGroup",
                        "logs:PutLogEvents"
                    ],
                    "Resource": "*"
                }
            ]
        }
    ),
    opts=pulumi.ResourceOptions(depends_on=[hello_role], parent=hello_role)
)

print(hello_role.id)
